import { CoolProjectPage } from './app.po';

describe('cool-project App', () => {
  let page: CoolProjectPage;

  beforeEach(() => {
    page = new CoolProjectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
